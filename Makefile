# Makefile for QUIET diffuse foregrounds paper
# $Id: Makefile 9 2010-05-14 19:35:28Z colin $

LATEX = pdflatex
BIBTEX = bibtex


TARGET = rslts

TEX = $(TARGET).tex
DVI = $(TARGET).dvi
CHAPTERS = tex/intro.tex tex/data.tex tex/patch_gc.tex tex/patch_gb.tex tex/cmb_patches.tex tex/conclusions.tex

all: $(TARGET).pdf

$(TARGET).pdf: $(TEX) ## $(CHAPTERS)
	$(LATEX) $(TEX)
#	$(BIBTEX) $(TARGET)
#	$(LATEX) $(TEX)
#	$(LATEX) $(TEX)
#	dvips -Ppdf $(DVI)
#	dvipdf $(DVI)	
.PHONY: clean
clean:
	rm -f *.aux
	rm -f $(TARGET).log
	rm -f $(TARGET).dvi
	rm -f $(TARGET).ps
	rm -f $(TARGET).pdf
	rm -f $(TARGET).lof
	rm -f $(TARGET).out
	rm -f $(TARGET).toc
	rm -f $(TARGET).bbl
	rm -f $(TARGET).blg
